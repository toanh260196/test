#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 13:11:59 2017

@author: guysimons
"""

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import csv
import pandas as pd
import xlsxwriter
import time

from selenium.webdriver.common.action_chains import ActionChains
 
 
def init_driver():

    driver = webdriver.Chrome(executable_path='/Users/guysimons/Documents/BISS/FuturizingBusinessSchools/Scraping Python/Drivers/chromedriver')
    #driver = webdriver.Safari()
    driver.wait = WebDriverWait(driver, 5)
    driver.implicitly_wait(10)
    return driver

def navigate_page(driver, page):
     driver.get(page)          
 
def lookup(driver, query):
     try:
          element = driver.find_element_by_xpath(query).text
          return element
     except:
          print("An error occured, the element doesn't exist")
        
 

driver = init_driver()


###Linkedin Login###
navigate_page(driver, "https://www.linkedin.com/")
email = driver.find_element_by_name("session_key")
password = driver.find_element_by_name("session_password")
email.send_keys("")
password.send_keys("")

login_button = driver.find_element_by_id("login-submit")
login_button.click()

###Navigate to page

#import file
liurl = pd.read_excel('/Users/guysimons/documents/biss/futurizingbusinessschools/data_sources_db/linkedinData.xlsx',header=None)
urls = liurl.loc[:,1].tolist()
universityID = liurl.loc[:,0].tolist()


for url in range(0,len(urls)):
     if (urls[url] == 'None'):
         continue

     navigate_page(driver,urls[url])
     
     time.sleep(5)
     
     ###Navigate to carreer insights
     career_insights = driver.find_element_by_xpath("//div[@class = 'org-alumni-preview-module__see-all-link']/a")
     actionchains1 = ActionChains(driver)
     actionchains1.double_click(career_insights).perform()

     time.sleep(5)     
     ###Show more
     show_more = driver.find_element_by_xpath("//button[@class='org-alumni-insights__show-more-button Sans-17px-black-55%-semibold']")
     actionchains2 = ActionChains(driver)
     actionchains2.double_click(show_more).perform()
 
     time.sleep(5)
     ###Get countries
     where_they_live = []
     where_they_live_values = []
     where_they_work = []
     where_they_work_values = []
     what_they_do = []
     what_they_do_values = []
     what_they_studied = []
     what_they_studied_values = []
     what_they_are_good_at = []
     what_they_are_good_at_values = []
       
     countries_names = driver.find_elements_by_xpath("//li[1]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']//span")
     countries_numbers = driver.find_elements_by_xpath("//li[1]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']/strong")
     for i in countries_names:
          where_they_live.append(i.text)
     
     for j in countries_numbers:
          where_they_live_values.append(j.text)
     
     work_names = driver.find_elements_by_xpath("//li[2]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']//span")
     work_numbers = driver.find_elements_by_xpath("//li[2]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']/strong")
     for i in work_names:
          where_they_work.append(i.text)
     
     for j in work_numbers:
          where_they_work_values.append(j.text)
     
     do_names = driver.find_elements_by_xpath("//li[3]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']//span")
     do_numbers = driver.find_elements_by_xpath("//li[3]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']/strong")
     for i in do_names:
          what_they_do.append(i.text)
     
     for j in do_numbers:
          what_they_do_values.append(j.text)
     
     next_btn = driver.find_element_by_xpath("//button[@class='next-btn']")
     actionchains3 = ActionChains(driver)
     actionchains3.double_click(next_btn).perform()
     time.sleep(5)

     
     study_names = driver.find_elements_by_xpath("//li[4]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']//span")
     study_numbers = driver.find_elements_by_xpath("//li[4]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']/strong")
     for i in study_names:
          what_they_studied.append(i.text)
     
     for j in study_numbers:
          what_they_studied_values.append(j.text)
     
     good_names = driver.find_elements_by_xpath("//li[5]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']//span")
     good_numbers = driver.find_elements_by_xpath("//li[5]//div[@class = 'insight-container']//div[@class='org-bar-graph-element__percentage-bar-info Sans-15px-black-70% mt2 mb4']/strong")
     for i in good_names:
          what_they_are_good_at.append(i.text)
     
     for j in good_numbers:
          what_they_are_good_at_values.append(j.text)
     
     id_seq = [universityID[url]]*len(where_they_live)       



     with open('resultsFinal1995.csv','a',encoding='utf-8-sig') as f:
         w = csv.writer(f)
         for i in range(0,15):
              w.writerow([id_seq[i],where_they_live[i], where_they_live_values[i], where_they_work[i], where_they_work_values[i],
                          what_they_do[i], what_they_do_values[i], what_they_studied[i], what_they_studied_values[i],
                          what_they_are_good_at[i], what_they_are_good_at_values[i]])   
        

driver.quit()
