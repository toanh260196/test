# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
from langdetect import detect

import cx_Oracle
db = cx_Oracle.connect('FBSADMIN', 'FA8skhg6', 'um003089.unimaas.nl:1521/FBSDB')

def detect_language(x):
   try:
       return detect(x.read())
   except:
       return 'No language detected.'

def change_time(x):
   s = list(x)
   s[10] = ' '
   del s[-5:]
   return "".join(s)

# Facebook comments
df = pd.read_sql("select FACEBOOK_COMMENTSPK,CREATED_TIME,MESSAGE,MESSAGELANGUAGE from facebook_comments_edited",db)
df['MESSAGELANGUAGE'] = df['MESSAGE'].apply(lambda x: detect_language(x))
df.drop('MESSAGE',axis=1)
df['CREATED_TIME'] = df['CREATED_TIME'].apply(lambda x: change_time(x))
df.to_sql('FB_comments_lang', db, if_exists='replace', index=False)

# Facebook posts
df2 = pd.read_sql("select FB_POST_PK,CREATED_TIME,MESSAGE,MESSAGELANGUAGE from FACEBOOK_POSTS_EDITED",db)
df2['MESSAGELANGUAGE'] = df2['MESSAGE'].apply(lambda x: detect_language(x))
df2.drop('MESSAGE',axis=1)
df2['CREATED_TIME'] = df2['CREATED_TIME'].apply(lambda x: change_time(x))
df2.to_sql('FB_posts_lang', db, if_exists='replace', index=False)